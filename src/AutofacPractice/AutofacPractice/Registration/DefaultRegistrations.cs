﻿namespace AutofacPractice.Registration
{
    /// <summary>
    /// https://autofac.readthedocs.io/en/latest/register/registration.html#default-registrations
    /// </summary>
    public class DefaultRegistrations
    {
        public class OtherServiceA : IServiceA
        {
            public void Dispose()
            {
            }
        }

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            //Autofac will use the last registered component as the default provider of that service:
            builder.RegisterType<ServiceA>().As<IServiceA>();
            builder.RegisterType<OtherServiceA>().As<IServiceA>();
            
            return builder.Build(); 
        }
    }
}
