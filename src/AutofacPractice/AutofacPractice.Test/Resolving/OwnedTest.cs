﻿using AutofacPractice.Resolving;
using AutofacPractice.Services;
using static AutofacPractice.Resolving.Owned;

namespace AutofacPractice.Test.Resolving
{
    [TestFixture]
    public class OwnedTest
    {
        [Test]
        public void shouldTestOwned()
        {
            var builder = new Owned();
            var container = builder.Configure();

            IDisposableServiceA injectedServiceA;
            IDisposableServiceB injectedserviceB;

            IDisposableServiceA serviceA;
            IDisposableServiceB serviceB;

            using (var scope = container.BeginLifetimeScope())
            {
                var owned = scope.Resolve<OwnedService>();
                injectedServiceA = owned.ServiceA.Value;
                injectedserviceB = owned.ServiceB;

                serviceA = scope.Resolve<IDisposableServiceA>();
                serviceB = scope.Resolve<IDisposableServiceB>();
            }

            injectedServiceA.IsDisposed.Should().BeFalse();
            injectedserviceB.IsDisposed.Should().BeTrue();

            serviceA.IsDisposed.Should().BeTrue();
            serviceA.IsDisposed.Should().BeTrue();
        }
    }
}
