﻿namespace AutofacPractice.Services
{
    public interface IDisposableServiceC : IDisposableService
    {

    }

    public class DisposableServiceC : IDisposableServiceC
    {
        public bool IsDisposed { get; private set; }
        public int DisposeCounter { get; private set; }

        public IDisposableServiceA ServiceA { get; private set; }

        public IDisposableServiceB ServiceB { get; private set; }

        public DisposableServiceC(IDisposableServiceA serviceA, IDisposableServiceB serviceB)
        {
            ServiceA = serviceA;
            ServiceB = serviceB;
        }

        public void Dispose()
        {
            IsDisposed = true;
            DisposeCounter++;
        }
    }
}
