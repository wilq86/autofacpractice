﻿using static AutofacPractice.Registration.OpenGenericComponents;

namespace AutofacPractice.Test.RegistrationTest
{
    [TestFixture]
    public class OpenGenericComponentsTest
    {
        [Test]
        public void shouldReturnGeneric()
        {
            var container = new OpenGenericComponents().Configure();

            var carRepository = container.Resolve<IRepository<Car>>();
            carRepository.Should().BeOfType<Repository<Car>>();    

            var userRepository = container.Resolve<IRepository<User>>();
            userRepository.Should().BeOfType<Repository<User>>();    
        }
    }
}
