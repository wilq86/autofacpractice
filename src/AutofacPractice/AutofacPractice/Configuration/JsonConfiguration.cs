﻿using Autofac.Configuration;
using Microsoft.Extensions.Configuration;

namespace AutofacPractice.Configuration
{
    public partial class JsonConfiguration
    {
        public IContainer Configure()
        {
            // Add the configuration to the ConfigurationBuilder.
            var config = new ConfigurationBuilder();
            // config.AddJsonFile comes from Microsoft.Extensions.Configuration.Json
            // config.AddXmlFile comes from Microsoft.Extensions.Configuration.Xml
            config.AddJsonFile(@"Configuration\autofac.json");

            // Register the ConfigurationModule with Autofac.
            var module = new ConfigurationModule(config.Build());
            var builder = new ContainerBuilder();
            builder.RegisterModule(module);

            return builder.Build();
        }
    }
}
