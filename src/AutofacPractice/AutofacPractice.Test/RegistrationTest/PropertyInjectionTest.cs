﻿using static AutofacPractice.Registration.PropertyInjection;

namespace AutofacPractice.Test.RegistrationTest
{
    [TestFixture]
    public class PropertyInjectionTest
    {
        [Test]
        public void shouldTestConfigurePropertiesAutowired()
        {
            var container = new PropertyInjection().ConfigurePropertiesAutowired();
            var service = container.Resolve<PropertyService>();

            service.Should().BeOfType<PropertyService>();
            service.ServiceA.Should().BeOfType<SimpleServiceA>();
            service.ServiceB.Should().BeOfType<SimpleServiceB>();
            service.ServiceC.Should().BeOfType<SimpleServiceC>();
            service.Value.Should().Be("Autowired");
        }

        [Test]
        public void shouldTestConfigurePropertiesByLambda()
        {
            var container = new PropertyInjection().ConfigurePropertiesByLambda();
            var service = container.Resolve<PropertyService>();

            service.Should().BeOfType<PropertyService>();
            service.ServiceA.Should().BeOfType<SimpleServiceA>();
            service.ServiceB.Should().BeOfType<SimpleServiceB>();
            service.ServiceC.Should().BeOfType<SimpleServiceC>();
            service.Value.Should().Be("ByLambda");
        }

        [Test]
        public void shouldTestConfigurePropertiesOnActivated()
        {
            var container = new PropertyInjection().ConfigurePropertiesOnActivated();
            var service = container.Resolve<PropertyService>();

            service.Should().BeOfType<PropertyService>();
            service.ServiceA.Should().BeOfType<SimpleServiceA>();
            service.ServiceB.Should().BeOfType<SimpleServiceB>();
            service.ServiceC.Should().BeOfType<SimpleServiceC>();
            service.Value.Should().Be("OnActivated");
        }
    }
}
