﻿namespace AutofacPractice.Test.RegistrationTest
{

    [TestFixture]
    public class DefaultLifeSpanTest
    {
        [Test]
        public void shouldAlwaysReturnThisSameObject()
        {
            var container = new DefaultLifeSpan().Configure();

            int hashCode;

            using (var scope = container.BeginLifetimeScope())
            {
                var service1 = scope.Resolve<IDisposableServiceA>();
                var service2 = scope.Resolve<IDisposableServiceA>();

                service1.GetHashCode().Should().Be(service2.GetHashCode());
                hashCode = service1.GetHashCode();
            }


            using (var scope = container.BeginLifetimeScope())
            {
                var service1 = scope.Resolve<IDisposableServiceA>();
                var service2 = scope.Resolve<IDisposableServiceA>();

                service1.GetHashCode().Should().Be(service2.GetHashCode());
                hashCode.Should().Be(service1.GetHashCode());

                //Can't be disposed
                ((DisposableServiceA)service1).IsDisposed.Should().BeFalse();
            }
        }
    }
}
