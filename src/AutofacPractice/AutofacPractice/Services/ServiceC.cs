﻿namespace AutofacPractice.Services
{
    public interface IServiceC { }

    public class ServiceC : IServiceC
    {
        public IServiceA ServiceA { get; private set; }

        public IServiceB ServiceB { get; private set; }

        public ServiceC(IServiceA serviceA, IServiceB serviceB)
        {
            ServiceA = serviceA;
            ServiceB = serviceB;
        }
    }
}
