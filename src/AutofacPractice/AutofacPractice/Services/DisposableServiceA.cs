﻿namespace AutofacPractice.Services
{
    public interface IDisposableServiceA : IDisposableService
    {

    }

    public class DisposableServiceA : IDisposableServiceA
    {
        public bool IsDisposed { get; private set; }

        public int DisposeCounter { get; private set; }

        public void Dispose()
        {
            IsDisposed = true;
            DisposeCounter++;
        }
    }
}
