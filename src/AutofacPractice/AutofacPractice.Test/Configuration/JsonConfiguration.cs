﻿
using AutofacPractice.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Registration.OpenGenericComponents;

namespace AutofacPractice.Test.Configuration
{
    [TestFixture]
    public class JsonConfigurationTest
    {
        [Test]
        public void shouldReadConfigFile()
        {
            var container = new JsonConfiguration().Configure();

            var service = container.Resolve<IServiceA>();
            service.Should().NotBeNull();
        }
    }
}
