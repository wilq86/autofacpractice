﻿namespace AutofacPractice.Registration
{

    public class PropertyInjection
    {
        public class PropertyService
        {
            public ISimpleServiceA? ServiceA { get; set; }

            public ISimpleServiceB? ServiceB { get; set; }

            public ISimpleServiceC? ServiceC { get; set; }

            public string Value { get; set; } = string.Empty;
        }

        public IContainer ConfigurePropertiesAutowired()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<SimpleServiceA>().As<ISimpleServiceA>();
            builder.RegisterType<SimpleServiceB>().As<ISimpleServiceB>();
            builder.RegisterType<SimpleServiceC>().As<ISimpleServiceC>();

            builder.RegisterType<PropertyService>()
                .PropertiesAutowired()
                .WithProperty("Value", "Autowired");

            return builder.Build();
        }

        public IContainer ConfigurePropertiesByLambda()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<SimpleServiceA>().As<ISimpleServiceA>();
            builder.RegisterType<SimpleServiceB>().As<ISimpleServiceB>();
            builder.RegisterType<SimpleServiceC>().As<ISimpleServiceC>();

            builder.Register(c => new PropertyService()
            {
                ServiceA = c.Resolve<ISimpleServiceA>(),
                ServiceB = c.Resolve<ISimpleServiceB>(),
                ServiceC = c.Resolve<ISimpleServiceC>(),
                Value = "ByLambda"
            });

            return builder.Build();
        }

        public IContainer ConfigurePropertiesOnActivated()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<SimpleServiceA>().As<ISimpleServiceA>();
            builder.RegisterType<SimpleServiceB>().As<ISimpleServiceB>();
            builder.RegisterType<SimpleServiceC>().As<ISimpleServiceC>();

            //By OnActivated
            builder.Register(c => new PropertyService())
                .OnActivated(e =>
                {
                    e.Instance.ServiceA = e.Context.Resolve<ISimpleServiceA>();
                    e.Instance.ServiceB = e.Context.Resolve<ISimpleServiceB>();
                    e.Instance.ServiceC = e.Context.Resolve<ISimpleServiceC>();
                    e.Instance.Value = "OnActivated";
                });

            return builder.Build();
        }

        
    }
}
