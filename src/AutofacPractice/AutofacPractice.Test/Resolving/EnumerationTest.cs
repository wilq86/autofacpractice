﻿using AutofacPractice.Resolving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacPractice.Test.Resolving
{
    [TestFixture]
    public class EnumerationTest
    {
        [Test]
        public void shouldTestEnumeration()
        {
            var container = new Enumeration().Configure();

            var enumeration = container.Resolve<IEnumerable<IDisposableService>>();
            enumeration.Count().Should().Be(3);
        }
    }
}
