﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Resolving.Owned;

namespace AutofacPractice.Resolving
{
    public class Enumeration
    {
        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<DisposableServiceA>()
                .AsImplementedInterfaces();

            builder.RegisterType<DisposableServiceB>()
                .AsImplementedInterfaces();

            builder.RegisterType<DisposableServiceC>()
                .As<IDisposableServiceC>()
                .As<IDisposableService>();

            return builder.Build();
        }
    }
}
