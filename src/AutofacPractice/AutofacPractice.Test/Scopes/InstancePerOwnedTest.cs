﻿using Autofac.Features.OwnedInstances;
using AutofacPractice.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Scopes.InstancePerDependency;

namespace AutofacPractice.Test.Scopes
{
    [TestFixture]
    public class InstancePerOwnedTest
    {
        [Test]
        public void shouldNotDiposeInLifetimeScope()
        {
            var container = new InstancePerOwned().Configure();
            Owned<IDisposableServiceA> service;

            using (var scope = container.BeginLifetimeScope())
            {
                service = scope.Resolve<Owned<IDisposableServiceA>>();
            }

            ((IDisposableServiceA)service.Value).IsDisposed.Should().BeFalse();
        }
    }
}
