﻿namespace AutofacPractice.Registration
{
    /// <summary>
    /// https://autofac.readthedocs.io/en/latest/register/scanning.html#assembly-scanning
    /// </summary>
    public class AModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.Register(c => new AComponent()).As<AComponent>();
        }
    }
}
