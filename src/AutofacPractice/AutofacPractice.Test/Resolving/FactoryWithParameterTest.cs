﻿using AutofacPractice.Resolving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Resolving.Factory;
using static AutofacPractice.Resolving.FactoryWithParameter;

namespace AutofacPractice.Test.Resolving
{
    [TestFixture]
    public class FactoryWithParameterTest
    {
        [Test]
        public void shouldTestFactory()
        {
            var container = new FactoryWithParameter().Configure();

            var factory = container.Resolve<FactoryWithParameterService>();

            IServiceA a = factory.Create("SomeValue");

            a.Should().BeOfType<ServiceAWithParameter>();
            ServiceAWithParameter serviceAWithParameter = (ServiceAWithParameter)a; 

            serviceAWithParameter.Value.Should().Be("SomeValue");
        }
    }
}
