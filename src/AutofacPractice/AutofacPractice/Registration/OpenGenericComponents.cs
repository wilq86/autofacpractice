﻿namespace AutofacPractice.Registration
{
    /// <summary>
    /// https://autofac.readthedocs.io/en/latest/register/registration.html#open-generic-components
    /// </summary>
    public class OpenGenericComponents
    {
        public interface IRepository<T>
        {
            T GetById(int id);  
        }

        public class Repository<T> : IRepository<T>
        {
            public T GetById(int id)
            {
                throw new NotImplementedException();
            }
        }

        public class Car { }

        public class CarRepository : Repository<Car> { }

        public class User { }

        public class UserRepository : Repository<User> { }

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>))
                .InstancePerLifetimeScope();

            return builder.Build();
        }
    }
}
