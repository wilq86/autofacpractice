﻿using static AutofacPractice.Registration.ResolveParameters;

namespace AutofacPractice.Test.RegistrationTest
{
    [TestFixture]
    public class ParametersTest
    {
        [Test]
        public void sholudConstrucBetterService()
        {
            var container = new ResolveParameters().Configure();
            var service = container.Resolve<IServiceA>(new NamedParameter("type", "better"));

            service.Should().BeOfType<BettersServiceA>();
        }

        [Test]
        public void sholudConstrucNormalService()
        {
            var container = new ResolveParameters().Configure();
            var service = container.Resolve<IServiceA>(new NamedParameter("type", "normal"));

            service.Should().BeOfType<ServiceA>();
        }

        [Test]
        public void sholudThrowException()
        {
            var container = new ResolveParameters().Configure();

            container.Invoking( c => c.Resolve<IServiceA>())
                .Should()
                .Throw<Exception>();
        }
    }
}
