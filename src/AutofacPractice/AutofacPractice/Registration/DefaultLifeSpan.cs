﻿namespace AutofacPractice.Registration
{
    public class DefaultLifeSpan
    {
        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            var service = new DisposableServiceA();

            //Can't be InstancePerLifetimeScope(); 
            //default lifespan is Transient
            builder.RegisterInstance(service).As<IDisposableServiceA>();

            return builder.Build();
        }
    }
}
