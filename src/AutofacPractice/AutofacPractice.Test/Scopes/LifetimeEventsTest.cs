﻿using AutofacPractice.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacPractice.Test.Scopes
{
    [TestFixture]
    public class LifetimeEventsTest
    {
        [Test]
        public void should()
        {
            var builder = new LifetimeEvents();
            var container = builder.Configure();

            builder.Preparing.Should().BeFalse();
            builder.Activating.Should().BeFalse();
            builder.Activated.Should().BeFalse();
            builder.Release.Should().BeFalse();

            using (var scope = container.BeginLifetimeScope())
            {
                scope.Resolve<IServiceA>();

                builder.Preparing.Should().BeTrue();
                builder.Activating.Should().BeTrue();
                builder.Activated.Should().BeTrue();
                builder.Release.Should().BeFalse();
            }

            builder.Preparing.Should().BeTrue();
            builder.Activating.Should().BeTrue();
            builder.Activated.Should().BeTrue();
            builder.Release.Should().BeTrue();
        }
    }
}
