﻿namespace AutofacPractice.Registration
{
    /// <summary>
    /// https://autofac.readthedocs.io/en/latest/register/registration.html#selection-of-an-implementation-by-parameter-value
    /// </summary>
    public class ResolveParameters
    {
        public class BettersServiceA : IServiceA
        {
            public bool IsDisposed { get; private set; }
            public int DisposeCounter { get; private set; }

            public void Dispose()
            {
                IsDisposed = true;
                DisposeCounter++;
            }
        }

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.Register<IServiceA>(
              (c, p) =>
                {
                    var accountId = p.Named<string>("type");
                    if (accountId.StartsWith("better"))
                    {
                        return new BettersServiceA();
                    }
                    else
                    {
                        return new ServiceA();
                    }
                });

            return builder.Build();
        }
    }
}
