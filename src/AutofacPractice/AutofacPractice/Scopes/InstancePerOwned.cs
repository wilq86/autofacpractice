﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Scopes.InstancePerDependency;

namespace AutofacPractice.Scopes
{
    public class InstancePerOwned
    {
        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<DisposableServiceA>()
                .As<IDisposableServiceA>();

            return builder.Build();
        }
    }
}
