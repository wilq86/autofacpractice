﻿namespace AutofacPractice.Services
{
    public interface IServiceB { }

    public class ServiceB : IServiceB
    {
        public IServiceA ServiceA { get; private set; }

        public ServiceB(IServiceA serviceA)
        {
            ServiceA = serviceA;
        }
    }
}
