﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacPractice.Test.Scopes
{
    [TestFixture]
    public class AddingRegistrationsTest
    {
        [Test]
        public void shouldRegisterTypes()
        {
            var container = new ContainerBuilder().Build();

            using (var scope = container.BeginLifetimeScope(
              builder =>
              {
                  builder.RegisterType<ServiceA>().As<IServiceA>();
              }))
            {
                var service = scope.Resolve<IServiceA>();
            }

            using (var scope = container.BeginLifetimeScope())
            {
                scope.Invoking(c => c.Resolve<IServiceA>()).Should().Throw<Exception>();
            }
        }
    }
}
