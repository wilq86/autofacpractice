﻿namespace AutofacPractice.Test.RegistrationTest
{
    [TestFixture]
    public class RegisterInstanceTest
    {
        [Test]
        public void shouldResolveAllService()
        {
            var container = new RegisterInstance().Configure();

            var serviceA = container.Resolve<IServiceA>();
            var serviceBByLambda = container.Resolve<IServiceB>();
            var serviceCByLambdaWithCtx = container.Resolve<IServiceC>();

            serviceA.Should().NotBeNull();
            serviceBByLambda.Should().NotBeNull();
            serviceCByLambdaWithCtx.Should().NotBeNull();  
        }
    }
}
