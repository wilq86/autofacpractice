﻿namespace AutofacPractice.Scopes
{
    public partial class DisablingDisposal
    {
        public class DisposableCountingServiceA : IServiceA
        {
            public static int DisposeCounter { get; private set; }

            public void Dispose()
            {
                DisposeCounter++;
            }
        }

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<DisposableCountingServiceA>()
                .As<IServiceA>().ExternallyOwned();

            return builder.Build();
        }
    }
}
