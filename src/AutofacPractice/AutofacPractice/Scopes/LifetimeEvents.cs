﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Scopes.SingleInstance;

namespace AutofacPractice.Scopes
{
    public class LifetimeEvents
    {
        public bool Preparing;
        public bool Activating;
        public bool Activated;
        public bool Release;

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ServiceA>()
                .As<IServiceA>()
                .OnPreparing(c => Preparing = true)
                .OnActivating(c => Activating = true)
                .OnActivated(c => Activated = true)
                .OnRelease(c => Release = true);

            return builder.Build();
        }
    }
}
