﻿using Autofac.Features.OwnedInstances;

namespace AutofacPractice.Resolving
{
    public class Owned
    {

        public class OwnedService
        {
            public Owned<IDisposableServiceA> ServiceA { get; set; }

            public IDisposableServiceB ServiceB { get; set; }

            public OwnedService(Owned<IDisposableServiceA> serviceA, IDisposableServiceB serviceB)
            {
                ServiceA = serviceA;
                ServiceB = serviceB;
            }
        }

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<DisposableServiceA>().As<IDisposableServiceA>().InstancePerLifetimeScope();
            builder.RegisterType<DisposableServiceB>().As<IDisposableServiceB>().InstancePerLifetimeScope();
            builder.RegisterType<OwnedService>().InstancePerLifetimeScope();

            return builder.Build();
        }
    }
}
