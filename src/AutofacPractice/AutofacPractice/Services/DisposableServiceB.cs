﻿namespace AutofacPractice.Services
{
    public interface IDisposableServiceB : IDisposableService
    {

    }

    public class DisposableServiceB : IDisposableServiceB
    {
        public bool IsDisposed { get; private set; }

        public int DisposeCounter { get; private set; }

        public IDisposableServiceA ServiceA { get; private set; }

        public DisposableServiceB(IDisposableServiceA serviceA)
        {
            ServiceA = serviceA;
        }

        public void Dispose()
        {
            IsDisposed = true;
            DisposeCounter++;
        }
    }
}
