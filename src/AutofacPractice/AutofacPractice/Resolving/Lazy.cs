﻿namespace AutofacPractice.Resolving
{
    public class Lazy 
    {
        public class LazyService
        {
            public Lazy<IServiceA> ServiceA { get; set; }

            public LazyService(Lazy<IServiceA> serviceA)
            {
                ServiceA = serviceA;
            }
        }

        //Owned
        public bool Activated = false;

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ServiceA>().As<IServiceA>().OnActivated(x => Activated = true);
            builder.RegisterType<LazyService>();

            return builder.Build();
        }
    }
}
