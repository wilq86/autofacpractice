﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Configuration.JsonConfiguration;
using static AutofacPractice.Scopes.InstancePerDependency;
using static AutofacPractice.Scopes.DisablingDisposal;
using AutofacPractice.Configuration;

namespace AutofacPractice.Test.Scopes
{
    [TestFixture]
    public class DisablingDisposalTest
    {
        [Test]
        public void shouldNeverDispose()
        {
            var container = new JsonConfiguration().Configure();

            for(int i= 0; i < 5; i++)
            {
                using (var scope = container.BeginLifetimeScope())
                {
                    for (int j = 0; j < 5; j++)
                    {
                        scope.Resolve<IServiceA>();
                    }
                }
            }

            DisposableCountingServiceA.DisposeCounter.Should().Be(0);
        }
    }
}
