﻿using AutofacPractice.Services;

namespace AutofacPractice.Registration
{
    public class RegisterInstance
    {
        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            var service = new ServiceA();

            //By lambda with context
            builder.Register(c => new ServiceA())
                .As<IServiceA>();

            //By lambda with resolving
            builder.Register((IServiceA serviceA) => new ServiceB(serviceA))
                .As<IServiceB>();

            //By lambda with both resolving nad context
            builder.Register((IComponentContext ctxt, IServiceA serviceA) => new ServiceC(serviceA, ctxt.Resolve<IServiceB>()))
                .As<IServiceC>();

            return builder.Build();
        }
    }
}
