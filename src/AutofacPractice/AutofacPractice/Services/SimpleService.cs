﻿namespace AutofacPractice.Services
{
    public interface ISimpleServiceA { }
    public class SimpleServiceA : ISimpleServiceA { }

    public interface ISimpleServiceB { }
    public class SimpleServiceB : ISimpleServiceB { }

    public interface ISimpleServiceC { }
    public class SimpleServiceC : ISimpleServiceC { }
}
