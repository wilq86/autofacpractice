﻿using Autofac.Features.OwnedInstances;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacPractice.Resolving
{
    public class Factory
    {
        public class FactoryService
        {
            public Func<IServiceA> ServiceA { get; set; }

            public FactoryService(Func<IServiceA> serviceA)
            {
                ServiceA = serviceA;
            }

            public IServiceA Create()
            {
                return ServiceA();
            }
        }

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ServiceA>().As<IServiceA>();
            builder.RegisterType<FactoryService>();

            return builder.Build();
        }
    }
}
