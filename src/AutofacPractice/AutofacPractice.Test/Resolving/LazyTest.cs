﻿using AutofacPractice.Resolving;
using static AutofacPractice.Resolving.Lazy;

namespace AutofacPractice.Test.Resolving
{
    [TestFixture]
    public class LazyTest
    {
        [Test]
        public void shouldTestLazy()
        {
            var builder = new Lazy();
            var container = builder.Configure();

            builder.Activated.Should().BeFalse();    
            var lazy = container.Resolve<LazyService>();
            builder.Activated.Should().BeFalse();    

            lazy.ServiceA.Should().NotBeNull();

            lazy.ServiceA.IsValueCreated.Should().BeFalse();
            lazy.ServiceA.Value.Should().NotBeNull();
            lazy.ServiceA.IsValueCreated.Should().BeTrue();

            builder.Activated.Should().BeTrue();    
        }
    }
}
