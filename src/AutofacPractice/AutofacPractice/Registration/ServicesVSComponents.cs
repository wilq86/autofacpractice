﻿namespace AutofacPractice.Registration
{
    /// <summary>
    /// https://autofac.readthedocs.io/en/latest/register/registration.html#services-vs-components
    /// </summary>
    public class ServicesVSComponents
    {
        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            //Registered only as ServiceA not as interface IServiceA
            builder.RegisterType<SimpleServiceA>();

            //Registered only as IServiceB not as ServiceB
            builder.RegisterType<SimpleServiceB>()
                .As<ISimpleServiceB>();

            //Registered as both
            builder.RegisterType<SimpleServiceC>()
                .AsSelf()
                .As<ISimpleServiceC>();

            return builder.Build();
        }
    }
}
