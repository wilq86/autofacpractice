﻿using static AutofacPractice.Registration.OpenGenericComponents;

namespace AutofacPractice.Scopes
{
    public class TaggingLifetimeScope
    {
        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<DisposableServiceA>()
                .As<IDisposableServiceA>()
                .InstancePerMatchingLifetimeScope("transaction");

            return builder.Build();
        }
    }
}
