﻿namespace AutofacPractice.Registration
{
    /// <summary>
    /// https://autofac.readthedocs.io/en/latest/register/parameters.html#passing-parameters-to-register
    /// </summary>
    public class RegisterParameters
    {
        public class ServiceWithParameter
        {
            public string Parameter { get; set; }

            public ServiceWithParameter(string parameter)
            {
                Parameter = parameter;
            }
        }

        public IContainer Confgiure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ServiceWithParameter>()
                .WithParameter("parameter", "ValueOfParameter");

            return builder.Build();
        }
    }
}
