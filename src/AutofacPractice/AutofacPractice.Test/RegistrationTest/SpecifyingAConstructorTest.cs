﻿namespace AutofacPractice.Test.RegistrationTest
{
    [TestFixture]
    public class SpecifyingAConstructorTest
    {

        [Test]
        public void sholudRunSecondConstructor()
        {
            var container = new SpecifyingAConstructor().Configure();
            var service = container.Resolve<IServiceC>();

            service.Should().NotBeNull();   
        }
    }
}
