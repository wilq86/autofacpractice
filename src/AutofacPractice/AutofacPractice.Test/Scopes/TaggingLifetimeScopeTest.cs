﻿using AutofacPractice.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacPractice.Test.Scopes
{
    [TestFixture]
    public class TaggingLifetimeScopeTest
    {
        [Test]
        public void shouldTestCreation()
        {
            var container = new TaggingLifetimeScope().Configure();

            using (var scope = container.BeginLifetimeScope("transaction"))
            {
                IDisposableServiceA service = scope.Resolve<IDisposableServiceA>();

                IDisposableServiceA serviceFromInnerScope1;
                using (var innerScope = container.BeginLifetimeScope())
                {
                    serviceFromInnerScope1 = scope.Resolve<IDisposableServiceA>();
                }

                IDisposableServiceA serviceFromInnerScope2;
                using (var innerScope = container.BeginLifetimeScope())
                {
                    serviceFromInnerScope2 = scope.Resolve<IDisposableServiceA>();
                }

                service.Should().Be(serviceFromInnerScope1)
                    .And.Be(serviceFromInnerScope2);

                service.IsDisposed.Should().BeFalse();
            }
        }
    }
}
