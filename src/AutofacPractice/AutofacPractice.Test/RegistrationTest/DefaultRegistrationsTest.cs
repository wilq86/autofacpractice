﻿using static AutofacPractice.Registration.DefaultRegistrations;

namespace AutofacPractice.Test.RegistrationTest
{
    [TestFixture]
    public class DefaultRegistrationsTest
    {
        [Test]
        public void shouldReturnLastRegisteredService()
        {
            var container = new DefaultRegistrations().Configure();

            var service = container.Resolve<IServiceA>();
            service.Should().BeOfType<OtherServiceA>();
        }
    }
}
