﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Scopes.InstancePerDependency;

namespace AutofacPractice.Scopes
{
    public class AsyncDispose
    {
        class AsyncDisposeComponent :  IAsyncDisposable
        {
            public bool IsDisposed { get; private set; }    

            public ValueTask DisposeAsync()
            {
                IsDisposed = true;
                return ValueTask.CompletedTask;
            }
        }

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<AsyncDisposeComponent>();

            return builder.Build();
        }
    }
}
