﻿using static AutofacPractice.Registration.RegisterParameters;

namespace AutofacPractice.Test.RegistrationTest
{
    [TestFixture]
    public class RegisterParametersTest
    {
        [Test]
        public void shouldInitWithParameter()
        {
            var container = new RegisterParameters().Confgiure();

            var service = container.Resolve<ServiceWithParameter>();

            service.Parameter.Should().Be("ValueOfParameter");
        }
    }
}
