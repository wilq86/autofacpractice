﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Scopes.InstancePerDependency;

namespace AutofacPractice.Scopes
{
    public class SingleInstance
    {
        public class SingleInstanceCountingService : IServiceA
        {
            public static int Counter;
            public SingleInstanceCountingService() => Counter++;
        }

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<SingleInstanceCountingService>()
                .As<IServiceA>()
                .SingleInstance();

            return builder.Build();
        }
    }
}
