﻿namespace AutofacPractice.Test.RegistrationTest
{
    [TestFixture]
    public class ServicesVSComponentsTest
    {
        [Test]
        public void shouldResolve()
        {
            var container = new ServicesVSComponents().Configure();

            container.Resolve<SimpleServiceA>();
            container.Invoking(c => c.Resolve<ISimpleServiceA>()).Should().Throw<Exception>();

            container.Resolve<ISimpleServiceB>();
            container.Invoking(c => c.Resolve<SimpleServiceB>()).Should().Throw<Exception>();

            container.Resolve<ISimpleServiceC>();
            container.Resolve<SimpleServiceC>();
        }
    }
}
