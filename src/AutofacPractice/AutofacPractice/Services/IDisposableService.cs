﻿namespace AutofacPractice.Services
{
    public interface IDisposableService : IDisposable
    {
        int DisposeCounter { get; }

        bool IsDisposed { get; }
    }
}