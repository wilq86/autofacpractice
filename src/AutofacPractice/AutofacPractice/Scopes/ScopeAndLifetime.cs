﻿namespace AutofacPractice.Scopes
{
    public class ScopeAndLifetime
    {
        public class Component
        {
            private readonly Dependency _dep;

            public string Name => _dep.Name;

            public Component(Dependency dep)
            {
                _dep = dep;
            }
        }

        public class Dependency
        {
            public string Name { get; }

            public Dependency(string name)
            {
                Name = name;
            }
        }
    }
}
