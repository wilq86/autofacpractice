﻿using static AutofacPractice.Scopes.ScopeAndLifetime;

namespace AutofacPractice.Test.Scopes
{

    /// <summary>
    /// https://autofac.readthedocs.io/en/latest/lifetime/index.html#example-singleton-lifetime
    /// </summary>
    [TestFixture]
    public class ScopeAndLifetimeTest
    {
        [Test]
        public void should()
        {
            var builder = new ContainerBuilder();

            // A singleton declared at container build
            // time will be owned by the root scope and get
            // all dependencies from the root scope.
            builder.RegisterType<Component>()
                   .SingleInstance();

            // Anything that resolves a Dependency from
            // the root lifetime scope will see the name
            // as 'root'
            builder.Register(ctx => new Dependency("root"));

            var container = builder.Build();

            // Resolve the component from the root lifetime scope.
            var rootComp = container.Resolve<Component>();

            // This will show "root"
            rootComp.Name.Should().Be("root");

            // Even if we override the Dependency in a child
            // scope, it'll still show "root"
            using (var childScope = container.BeginLifetimeScope(b => b.Register(ctx => new Dependency("child1"))))
            {
                var child1Comp = childScope.Resolve<Component>();
                child1Comp.Name.Should().Be("root");
            }

            // You can override the singleton by adding a new
            // singleton in a child scope. This one will be
            // owned by the child and will work in its children,
            // but it doesn't override at the root scope.
            using (var child2Scope = container.BeginLifetimeScope(
              b =>
              {
                  b.RegisterType<Component>().SingleInstance();
                  b.Register(ctx => new Dependency("child2"));
              }))
            {
                var child2Comp = child2Scope.Resolve<Component>();

                // This will show "child2"
                child2Comp.Name.Should().Be("child2");

                // rootComp and child2Comp are TWO DIFFERENT SINGLETONS.
                rootComp.Should().NotBe(child2Comp);

                using (var child2SubScope = child2Scope.BeginLifetimeScope(
                  b => b.Register(ctx => new Dependency("child2SubScope"))))
                {
                    var child2SubComp = child2SubScope.Resolve<Component>();

                    // This will show "child2"
                    child2Comp.Name.Should().Be("child2");

                    // child2Comp and child2SubComp are THE SAME.
                    child2Comp.Should().Be(child2SubComp);
                }
            }
        }
    }
}
