﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacPractice.Scopes
{
    public partial class InstancePerDependency
    {
        public class CountingService : ServiceA
        {
            public static int Counter;
            public CountingService() => Counter++;
        }

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<CountingService>()
                .As<IServiceA>();

            return builder.Build();
        }
    }
}
