﻿namespace AutofacPractice.Registration
{
    public class SpecifyingAConstructor
    {

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ServiceA>().As<IServiceA>();
            builder.RegisterType<ServiceB>().As<IServiceB>();

            builder.RegisterType<ServiceC>()
                .As<IServiceC>()
                .UsingConstructor(typeof(IServiceA), typeof(IServiceB));

            return builder.Build();
        }
    }
}
