﻿using AutofacPractice.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Scopes.SingleInstance;

namespace AutofacPractice.Test.Scopes
{
    [TestFixture]
    public class SingleInstanceTest
    {
        [Test]
        public void shouldCreateOnlyOne()
        {
            var container = new SingleInstance().Configure();

            for(int i= 0; i < 5; i++)
            {
                using (var scope = container.BeginLifetimeScope())
                {
                    for (int j = 0; j < 5; j++)
                    {
                        scope.Resolve<IServiceA>();
                    }
                }
            }

            SingleInstanceCountingService.Counter.Should().Be(1); 
        }
    }
}
