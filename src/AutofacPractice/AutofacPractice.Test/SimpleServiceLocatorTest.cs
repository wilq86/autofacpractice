using static AutofacPractice.SimpleServiceLocator;

namespace AutofacPractice.Test
{
    public class SimpleServiceLocatorTest
    {
        [Test]

        public void shouldResolveSomeService()
        {
            var simple = new SimpleServiceLocator();
            simple.Configure();

            using (var scope = simple.Container!.BeginLifetimeScope())
            {
                var service1 = scope.Resolve<IService>();
                var service2 = scope.Resolve<SomeService>();

                (service1 is SomeService).Should().Be(true);
                (service2 is SomeService).Should().Be(true);
            }
        }


        //using (var scope = simple.Container.BeginLifetimeScope())
        //{
        //    var service1 = scope.Resolve<IService>();
        //    var service2 = scope.Resolve<IService>();

        //    service1.GetHashCode().Should().Be(service2.GetHashCode());
        //}
    }
}