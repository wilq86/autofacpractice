﻿namespace AutofacPractice
{
    public class SimpleServiceLocator
    {
        public interface IService
        {
        }

        public class SomeService : IService
        {

        }

        public IContainer? Container { get; private set; }  

        public void Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<SomeService>().As<IService>().InstancePerLifetimeScope();

            builder.RegisterType<SomeService>().AsSelf().As<IService>().InstancePerLifetimeScope();

            Container = builder.Build();
        }
    }
}
