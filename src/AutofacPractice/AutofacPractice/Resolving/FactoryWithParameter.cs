﻿using Autofac.Features.OwnedInstances;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacPractice.Resolving
{
    public class FactoryWithParameter
    {
        public class ServiceAWithParameter: IServiceA
        {
            public string Value { get; private set; }
            public ServiceAWithParameter(string value)
            {
                Value = value;
            }
        }

        public class FactoryWithParameterService
        {
            public Func<string, IServiceA> ServiceA { get; set; }

            public FactoryWithParameterService(Func<string, IServiceA> serviceA)
            {
                ServiceA = serviceA;
            }

            public IServiceA Create(string value)
            {
                return ServiceA(value);
            }
        }

        public IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ServiceAWithParameter>().As<IServiceA>();
            builder.RegisterType<FactoryWithParameterService>();

            return builder.Build();
        }
    }
}
