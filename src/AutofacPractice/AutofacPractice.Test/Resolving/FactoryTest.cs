﻿using AutofacPractice.Resolving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Resolving.Factory;

namespace AutofacPractice.Test.Resolving
{
    [TestFixture]
    public class FactoryTest
    {
        [Test]
        public void shouldTestFactory()
        {
            var container = new Factory().Configure();

            var factory = container.Resolve<FactoryService>();

            var a1 = factory.Create();
            var a2 = factory.Create();
            
            a1.Should().NotBe(a2);
        }
    }
}
