﻿using AutofacPractice.Scopes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AutofacPractice.Scopes.InstancePerDependency;

namespace AutofacPractice.Test.Scopes
{
    [TestFixture]
    public class InstancePerDependencyTest
    {
        [Test]
        public void shouldCrateEachTimeNewOne()
        {
            var container = new InstancePerDependency().Configure();

            for(int i= 0; i < 5; i++)
            {
                using (var scope = container.BeginLifetimeScope())
                {
                    for (int j = 0; j < 5; j++)
                    {
                        scope.Resolve<IServiceA>();
                    }
                }
            }

            CountingService.Counter.Should().Be(25); 
        }
    }
}
